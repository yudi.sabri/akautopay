try:
    import newrelic.agent

    newrelic.agent.initialize()
except Exception as e:
    print(f"Newrelic error: {e}")

try:
    import sentry_sdk
    from sentry_sdk.integrations.celery import CeleryIntegration
    from sentry_sdk.integrations.flask import FlaskIntegration
    from sentry_sdk.integrations.redis import RedisIntegration
    from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

    sentry_sdk.init(
        integrations=[
            FlaskIntegration(),
            RedisIntegration(),
            SqlalchemyIntegration(),
            CeleryIntegration(),
        ],
    )
except Exception as e:
    print(f"Failed to load sentry SDK : {e}")

# import atexit
# from datetime import datetime

import http
import importlib
import json
import locale
import os
from logging import getLogger
from typing import Any, Tuple

from akuser.cli_message_broker import pubsub_cli
from celery import Celery
from flask import Flask, Response, jsonify, request
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from .cli import email_cli, kafka_cli, user_cli

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
mail = Mail()

app_logger = getLogger("app")
error_logger = getLogger("error")
access_logger = getLogger("access")
performance_benchmark_logger = getLogger("performance_benchmark")
kafka_logger = getLogger("kafka")
wavecell_logger = getLogger("wavecell")
privy_logger = getLogger("privy")
clevertap_logger = getLogger("clevertap")


clevertap_dateformat = "%d-%m-%Y %H:%M:%S"

try:
    locale.setlocale(locale.LC_TIME, "id_ID.utf8")
except locale.Error as e:
    error_logger.error(f"LOCALE ERROR: {e}")
    pass


def create_app(test: bool = False) -> Flask:
    """
    This method used to handle app creation and configure some objects

    Keyword Arguments:
        test {bool}
          -- [The test flag to distinguish between main app and test app]
          (default: {False})

    Returns:
        Flask -- [Configured Flask App]
    """
    app = Flask(__name__, instance_relative_config=False)

    if test:
        app.config.from_object("akuser.config.TestConfig")
    else:
        app.config.from_object("akuser.config.Config")

    with app.app_context():
        scan_models(app.name)
        db.init_app(app)
        ma.init_app(app)
        migrate.init_app(app, db)
        mail.init_app(app)
        make_cli(app)

        blueprints = discover_blueprints(os.path.dirname(os.path.abspath(__file__)))

        for blueprint in blueprints:
            try:
                app.register_blueprint(blueprint)
            except Exception as e:
                error_logger.error(f"Failed to register blueprint {blueprint}: {e}")

        app.before_request(handle_options_method)

        app.register_error_handler(Exception, exception_handler)
        app.register_error_handler(http.HTTPStatus.NOT_FOUND, resource_not_found)
        app.register_error_handler(http.HTTPStatus.UNAUTHORIZED, unauthorized)
        app.register_error_handler(http.HTTPStatus.FORBIDDEN, forbidden)
        app.register_error_handler(
            http.HTTPStatus.METHOD_NOT_ALLOWED, method_not_allowed
        )

        return app


def make_cli(app: Flask) -> None:
    app.cli.add_command(pubsub_cli)
    app.cli.add_command(kafka_cli)
    app.cli.add_command(user_cli)
    app.cli.add_command(email_cli)

    @app.cli.command("check")
    def check():
        print("Everything run successfully")


def discover_blueprints(path: str) -> list:
    """
    This method used to load blueprints from given path

    Arguments:
        path {str} -- [The path that contains blueprints module]

    Returns:
        list -- [The list of blueprints object]
    """
    blueprints = list()
    dir_name = os.path.basename(path)
    packages = os.listdir(f"{path}/blueprints")

    for package in packages:
        if str(package).endswith(".py") and str(package) != "__init__.py":
            package = str(package).replace(".py", "")
            module_name = f"{dir_name}.blueprints.{package}"
            module = importlib.import_module(module_name)
            module_blueprints = [bp for bp in dir(module) if bp.endswith("_blueprint")]

            for mb in module_blueprints:
                blueprints.append(getattr(module, mb))

    return blueprints


# Request Handler
def handle_options_method() -> Any:
    """
    This method used to handle options method from front end

    Returns:
        Any -- [Return 204 if method is OPTIONS else return the original request]
    """
    if request.method == "OPTIONS":
        return Response(status=http.HTTPStatus.NO_CONTENT)


def after_request(response: Response) -> Response:
    """
    This method used to handle access log

    Arguments:
        response {Response} -- [The flask Response object]

    Returns:
        Response -- [Flask response object that passed to parameter]
    """
    try:
        try:
            request_payload_data = request.get_json() if request.get_json() else {}
        except Exception:
            request_payload_data = {}

        if request_payload_data and request_payload_data.get("password"):
            request_payload_data.pop("password")

        access_logger = getLogger("access")
        request_method = request.method
        request_headers = dict(request.headers)
        request_url = f"{request.scheme}://{request.remote_addr}{request.full_path}"
        request_payload = json.dumps(request_payload_data)

        response_headers = dict(response.headers)
        response_payload = json.loads(response.response[0]) if response.response else {}
        response_status = response.status
        message = (
            f"{request_method} | {request_url} | {response_status} "
            f"| {request_headers} | {request_payload} | {response_headers} "
            f"| {response_payload}"
        )
        access_logger.info(message)
    except Exception as e:
        exception_handler(e)

    return response


# ERROR HANDLER
def exception_handler(e: Exception) -> Response:
    """
    This method used to handle unhandled exception

    Arguments:
        e {Exception} -- [Exception object]

    Returns:
        Response -- [Return internal server error message]
    """
    error_logger.exception(e)

    return (
        jsonify({"code": 500, "message": str(e)}),
        http.HTTPStatus.INTERNAL_SERVER_ERROR,
    )


def resource_not_found(e: Any = "Not Found") -> Tuple[Any, http.HTTPStatus]:
    """
    This method is a error handler for http status Not Found

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Not Found"})

    Returns:
        [Response] -- [404 Response]
    """
    return jsonify({"code": 404, "message": str(e)}), http.HTTPStatus.NOT_FOUND
    # return make_json_response(http_status=404, data={"code": 404, "message": str(e)})


def unauthorized(e: Any = "Unauthorized") -> Tuple[Any, http.HTTPStatus]:
    """
    This method is a error handler for http status Unauthorized

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Unauthorized"})

    Returns:
        [Response] -- [401 Response]
    """
    return (
        jsonify({"code": 401, "message": str(e)}),
        http.HTTPStatus.UNAUTHORIZED,
    )
    # return make_json_response(http_status=401, data={"code": 401, "message": str(e)})


def forbidden(e: Any = "Forbidden") -> Tuple[Any, http.HTTPStatus]:
    """
    This method is a error handler for http status Forbidden

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Forbidden"})

    Returns:
        [Response] -- [403 Response]
    """
    return jsonify({"code": 403, "message": str(e)}), http.HTTPStatus.FORBIDDEN
    # return make_json_response(http_status=403, data={"code": 403, "message": str(e)})


def method_not_allowed(
    e: Any = "Method Not Allowed",
) -> Tuple[Any, http.HTTPStatus]:
    """
    This method is a error handler for http status Method Not Allowed

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Method Not Allowed"})

    Returns:
        [Response] -- [403 Response]
    """
    return (
        jsonify({"code": 405, "message": str(e)}),
        http.HTTPStatus.METHOD_NOT_ALLOWED,
    )
    # return make_json_response(http_status=405, data={"code": 405, "message": str(e)})


def scan_models(app_name: str):
    for dirpath, dirnames, filenames in os.walk(f"./{app_name}/models"):
        head, tail = os.path.split(dirpath)
        if tail == "models":
            for filename in filenames:
                if filename.endswith(".py") and filename != "__init__.py":
                    filename_no_ext, _ = os.path.splitext(
                        os.path.join(dirpath, filename)
                    )
                    filename_no_ext = filename_no_ext[2:]
                    module_path = filename_no_ext.replace(os.sep, ".")
                    module_path = module_path.replace("/", ".")
                    importlib.import_module(module_path)


def make_celery(app=None) -> Celery:
    app = app or create_app()
    celery_instance = Celery(
        app.import_name,
        backend=app.config.get("CELERY_RESULT_BACKEND"),
        broker=app.config.get("CELERY_BROKER_URL"),
    )
    celery_instance.conf.update(app.config)

    celery_instance.conf.update(
        {
            "CELERY_ROUTES": {
                "send_email_kyc_reminder": {"queue": "send_kyc_update_reminder"}
            },
        }
    )

    class ContexTask(celery_instance.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_instance.Task = ContexTask
    return celery_instance
