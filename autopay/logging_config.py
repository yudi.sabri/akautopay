import os

from akseleran.common import EnvConfig

env = EnvConfig("AUTOPAY")
here = os.path.abspath(os.path.dirname(__file__))

LOGGING_CONFIG = {
    "version": 1,
    "formatters": {
        "simple": {"format": "%(asctime)s | %(name)s | %(levelname)s | %(message)s"},
    },
    "handlers": {
        "file_error": {
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 1000000000,
            "backupCount": 5,
            "level": "ERROR",
            "formatter": "simple",
            "filename": os.path.join(here, os.pardir, "logs", "error.log"),
        },
        "file_app": {
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 1000000000,
            "backupCount": 5,
            "level": "INFO",
            "formatter": "simple",
            "filename": os.path.join(here, os.pardir, "logs", "app.log"),
        },
        "file_access": {
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 1000000000,
            "backupCount": 5,
            "level": "INFO",
            "formatter": "simple",
            "filename": os.path.join(here, os.pardir, "logs", "access.log"),
        },
    },
}
