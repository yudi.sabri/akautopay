import os
from logging.config import dictConfig

from akseleran.common.env import EnvConfig

from .logging_config import LOGGING_CONFIG

env = EnvConfig("AUTOPAY")
here = os.path.abspath(os.path.dirname(__file__))


class Config:
    _basedir = os.path.abspath(os.path.dirname(__file__))

    SECRET = env.string("SECRET", "not-so-secret")

    FLASK_DEBUG = env.boolean("DEBUG", False)

    UTC_OFFSET = 7

    AKSELERAN_SIGNER_ROLE_ID = [8, 3]

    # Database Config
    DB_NAME = env.string("DB_NAME", "autopay")
    DB_USER = env.string("DB_USER", "postgres")
    DB_PASS = env.string("DB_PASS", "yudisabri123")
    DB_HOST = env.string("DB_HOST", "localhost")
    DB_PORT = env.string("DB_PORT", "5432")

    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}
    SQLALCHEMY_DATABASE_URI = (
        "postgresql+psycopg2://" f"{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_TIMEOUT = env.int("SQLALCHEMY_POOL_TIMEOUT", 60)
    SQLALCHEMY_POOL_SIZE = env.int("SQLALCHEMY_POOL_SIZE", 50)
    SQLALCHEMY_POOL_RECYCLE = env.int("SQLALCHEMY_POOL_SIZE", 120)
    SQLALCHEMY_MAX_OVERFLOW = env.int("SQLALCHEMY_MAX_OVERFLOW", 50)
   
    # Redis Config
    REDIS_HOST = env.string("REDIS_HOST", "localhost")
    REDIS_PORT = env.string("REDIS_PORT", "6379")
    REDIS_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}"
    REDIS_MASTER_DB = env.string("REDIS_MASTER_DB", "0")

    dictConfig(LOGGING_CONFIG)

    AKSELERAN_TEMPLATE_PATH = "akuser/templates/html"


class TestConfig(Config):
    """
    This class handle configuration for the test app
    """

    DB_TEST_PORT = env.string("DB_TEST_PORT", "5432")
    DB_TEST_PASS = env.string("DB_TEST_PASS", "password")
    DB_TEST_USER = env.string("DB_TEST_USER", "akuser")
    DB_TEST_NAME = env.string("DB_TEST_NAME", "akuser")
    DB_TEST_HOST = env.string("DB_TEST_HOST", "localhost")

    SQLALCHEMY_DATABASE_URI = (
        f"postgresql+psycopg2://"
        f"{DB_TEST_USER}:{DB_TEST_PASS}@{DB_TEST_HOST}:{DB_TEST_PORT}/{DB_TEST_NAME}"
    )
