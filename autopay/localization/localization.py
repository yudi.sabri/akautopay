import gettext

from flask import current_app


class Localization:
    def translate(word=None, lang="id"):

        if lang not in ["id", "en"]:
            lang = "id"

        localedir = current_app.config.get("LOCALIZATION_LOCALE_DIR")
        locale = gettext.translation("base", localedir=localedir, languages=[lang])
        locale.install()
        _ = locale.gettext
        translation = _(word)

        return translation
