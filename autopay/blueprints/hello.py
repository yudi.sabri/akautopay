
from flask import Blueprint
from flask_restful import Api
from autopay.resources.hello import HelloPageResource

landing_blueprint = Blueprint("landing", __name__, url_prefix="/landing")

landing_resources = Api(landing_blueprint)
landing_resources.add_resource(HelloPageResource, "/hi")