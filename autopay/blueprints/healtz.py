from akseleran.helpers.http_responses import make_json_response

import psycopg2
from autopay import error_logger
from flask import Blueprint, Response, current_app
from kafka import KafkaConsumer
from redis import Redis

healthz_blueprint = Blueprint("healthz", __name__, url_prefix="/healthz")



def check_redis():
    redis_host = current_app.config.get("REDIS_HOST")
    r = Redis(redis_host, socket_connect_timeout=60)  # short timeout for the test

    try:
        r.ping()
        return True
    except Exception as ex:
        error_logger.exception(ex)
        return False


def check_db():
    DB_NAME = current_app.config.get("DB_NAME")
    DB_USER = current_app.config.get("DB_USER")
    DB_PASS = current_app.config.get("DB_PASS")
    DB_HOST = current_app.config.get("DB_HOST")
    DB_PORT = current_app.config.get("DB_PORT")
    try:
        conn = psycopg2.connect(
            host=DB_HOST, port=DB_PORT, user=DB_USER, password=DB_PASS, database=DB_NAME
        )
        cur = conn.cursor()
        cur.execute("SELECT 1")
        cur.close()
        conn.close()
        return True

    except Exception as ex:
        error_logger.exception(ex)
        return False


@healthz_blueprint.route("", methods=("GET",))
def healthz() -> Response:
    """
    This method response hello world in /_healthz path

    Returns:
        [Response] -- [flask Response object]
    """
    try:
        red = check_redis()
        dat = check_db()

        if check_redis() is True and check_db() is True:
            return make_json_response(
                http_status=200,
                data={
                    "status": {"cache": red, "database": dat},
                    "info": "Server healthy! No Error Detected",
                },
            )
        else:
            return make_json_response(
                http_status=500,
                data={
                    "status": {"cache": red, "database": dat},
                    "info": "Something went wrong!",
                },
            )
    except Exception as ex:
        error_logger.error(ex)
        return make_json_response(
            http_status=500,
            data={
                "status": {"cache": None, "database": None, "messaging": None},
                "info": "Something went wrong!",
            },
        )
