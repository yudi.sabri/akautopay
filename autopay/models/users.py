from .base_models import BaseModel

import sqlalchemy as sa

class User(BaseModel):
    __tablename__ = "users"
    email = sa.Column(sa.String(500), nullable=True)
    fname = sa.Column(sa.String(255), nullable=True)
    lname = sa.Column(sa.String(255), nullable=True)