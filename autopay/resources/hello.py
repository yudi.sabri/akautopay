from flask_restful import Resource
from flask import Response, g, request
from autopay.tools.response import ok_message, make_json_response

class HelloPageResource(Resource):
    """Handle Landing Page"""
    def get(self):

        status, data = ok_message("hehe")
        return make_json_response(status_code=200, data=data)